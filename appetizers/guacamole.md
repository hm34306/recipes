# Guacamole
This is a very common at our house and when we have it is goes quickly! Lots of flavor, some heat. :)

We will use limes to keep the guacamole for oxidizing. I've been doing this long enough to know that storing the pit does absolultely nothing. Lime juice does a much better job and adds a nice acidic touch that blends well with all the other fresh ingredients.

We'll be smashing the gucamole with a large fork. I like the texture of a hand smashjed avacado. If you plan on using a blender, you are at the wrong site.

While I provide how we make this, this is really an appetizer that you can tune to your liking. So play with it and discover how you like your guacamole!

## Difficulty
Easy

## Prep Time
15 Minutes

## Ingredients
* 4 large Haas Avacodoes
    * Purchase a ripe avacado. If you slightly press on the outside, you should get a dimple. 
        * If you have to force a dimple, itis not ready and your asking for more work. 
        * If you get a large dimple, make sure you use it that day. It is ready now!
* Salt
* Garlic powder
* 1/2 Medium Onion, finly minced
    * If you really like the taste of onions (I do!) then do a full.
* 1 Medium tomatoe, inside extracted, finley chopped
    * I remove the watery inside so as not to have a watery guacamole.
* 1/2 hand full of chopped Cilantro
* 1 Fresh Jalapeno finely chopped
    * Or spicier chile if you so desire. If kids are eating this, go with a fresh Jalepeno.
* 1-2 Medium, Juicy limes
    * This depends on exacltly how juicy the limes are and how much you enjoy the lime flavor.

## Optional Ingredients
* Cojita Cheese
    * A little more texture, a tad more flavor
* Pumpkin seeds
    * My wife is not a fan, but I think this is a great way to add texture with out adding too much nuttiness flavor

# Steps
## Prepare the Avacados
1. Cut avacado in half, curring arojund the core.
2. Remove the core.
3. With a fork, core out the avacado into a bowl.
4. With a fork, corasely smash the avacado.
5. Add Salt to taste.
    * Yes you will need to taste it.
6. Add garlic powder to taste.
    * We like ours garlicy. How muc his up to you.
7. Add lime juice and fold in.

This should tase great on its own. Let's kick it up with some fresh ingredients!

## Add Fresh Ingredients
1. Add all of the copped incredients.

## Add Optional Ingredients
1. Cruble in the cojita cheese.
    * I typically crumble enough to mostly cover the top of the bowl.
2. Add pumpkin seed si you wish.

## Fold slowly
The goal so far has been to make a righ tich and creamy, not watery, guacamole. Let's slowly fold all out ingredients.

# Take care of you guacamole
If you make it ahead of time or can't eat it all in one sitting, cover your guacamole with saran wran wrap. Push down on the guacamole to get all the air out. Put it 

# It's time to eat!
Enyoy with chips or on tacos!

You've done the work, now enjoy!
