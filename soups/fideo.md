# Fideo (Sopa de Fideo)

This is simple, yet very delicious soup. Turth be told, I do not make mine very soupy. I love the smell when cooking this dish. All the aromatics really take me back to my childhood.

## Difficulty

Easy

## Ingredients

* 20 oz Fideo
* 1 Bell Pepper (*diced*)
* 1 Medium Onion (*diced*)
* 1 Serranos (*hotter*) or Jalapeños (*not as hot*) or None (*Kids may not want the heat!*) (*finely chopped*)
* 1 15 oz Crushed Tomatoes
* 1 tbs Garlic Powder
* 1 tbs Ground Comino
* 1 tbs Black Pepper
* 1 tps Salt to taste (you can always add more... just hard to remove)
* Cilantro - a hand full chopped
* Water

## Instructions

1. Add enough olive oil to cover the bottom of the pan and heat up.
2. Add noodles and pan fry.
   * You will want to use tongs to lift and stir the noodles.
   * Ensure all the noodles get coated in olive oil.
   * The more browned the noodles, the more flavor you are adding.
3. Once browned, put onions, bell pepper and  serranos.
4. Pan fry for 1-2 minutes.
5. Put in crushed tomatoes and let simmer for another 30 seconds.
6. Add enough water to cover the fideo pls about 2 centimeters.
   * If you want your fideo more soupy, then add more water. There is not harm in adding more.
7. Add garilc, comino, cilantro, salt and pepper and give a gnetle stir.
8. Boil for around 8 minutes or until noodles are aldente.

Enjoy your fideo!
