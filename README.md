# Welcome
These are recipes that I have gathered over time. Most of them are from my mom, Maggie, who ran a little taco stand called *Maggies Fast Tacos* for 17 years. I ate tacos most every day and still eat tacos as much as I can today. My grandma and tia had some influence in my mom's cooking as well, which thankfully I picked up.

## Measurements
I started cooking when I was 9. My mom did not measure much and I kind of picked up that habit as well. I'll do my best to add measurements, but do understand it is a bit of a estimate. For me enjoying the food is finding that taste your looking for. So feel free to add a pinch here or there or even remove as you see fit.

## Smells
Cooking and smells seem to go hand and hand. I'm always searching for that smell of when the onions and bell pepper hit the browning rice; I lean over,  take a whiff and it takes me back to my youth. If you can't smell it, its probably lacking something.

## Basic Ingredients
Most of the ingredients I use are fairly basic. I can typically get by with what is listed below. I do use other items, like Chicharrones or honey comb tripe, and they will show up as recipes call for it.

### Meats
* Chicken
* Ground Beef
* Pork

### Fruits and Vegetables
* Avocados
* Cilantro
* Green Bell pepper
* Jalapeños (Fresh)
* Limes
* Onions
* Potatoes
* Serranos
* Thai Chilis *Caliente*
* Tomatillos
* Tomatoes

### Staples
* Rice
* Beans
* Potatoes
* Flour Tortillas
* Corn Tortillas

### Canned Goods
* Tomato sauce
* Tomatillos
* Diced Tomatoes
* Crushed Tomatoes
* Chipotle in Adobo Sauce

### Spices
* Garlic Powder
* Ground Cumin 
* Salt
* Black Pepper
* Fancy Red Chili Powder
* Oregano

### Oils
* Canola Oil
* Lard