# New Mexico Red Salsa

We like ot go to New Mecico and I really like thier take on a a tick, sort of bitter Red Salsa. It is a different flavor profile as compared to a more tomatoe based salsa incorperating dried red chillies

## Difficulty

Easy

## Ingredients

* 2 medium vine Tomatoes cut into quarters
* 2 medium Onions cut into 6 pieces
* 1/8 Cup of Chii Pequin or Dried Red Chillies (Chile de Arbol)
* 2 Cascavel Drived Chilies
* 2 Ancho Dried Chilies
* 2 Guajillo Dried Chillies
* 2 tbs Minced Garlic (or 1 tbs garlic powder)
* 1 tbs Black Pepper
* 1 tps Salt to taste (you can always add more... just hard to remove)
* Cilantro - a hand full when it is time to blend

### Optional

* Substitute 2 Chipotle Dried Chiles for a smokey flavor
* If you like it spicier, add more Chii Pequin or Dried Red Chillies

## Instructions

1. Put Dried chilies in a pan and warm them up to bring out the pepper oils
2. Put all ingredients into a small pot
3. Add water to top of ingredients 
    * less water = thicker salsa, up to you
    * I Like a thicker salsa
4. Boil for 15 minutes
    * enough time for the chilies to turn a pale green
5. With a hand blender, blend away
    * I typically blend thoroughly
    * *NOTE:* If you prefer chunky, a food processor with some pulses is ideal

Enjoy your salsa, just don’t burn yourself...
