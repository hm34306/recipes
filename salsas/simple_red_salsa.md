# Simple Red Salsa
We eat salsa almost daily. We go through 6 or so bags of tortilla chips a week. My daughter Belicia eats chips and salsa for breakfast if you let her. Ariana always asks me to make it hotter. This is partly a recipe from my mom’s taco stand and partly me learning some things along the way.

## Difficulty
Easy - Belicia, my 9 year old makes this now :)

## Ingredients
* 2 medium vine Tomatoes cut into quarters
* 2 medium Onions cut into 6 pieces
* 4 Serranos (hotter) or Jalapeños (fresh, not as hot) or a combination
   * add more if you so dare
   * add Cayenne, though it does change the flavor a bit
* 15 oz Crushed Tomatoes
* 2 tbs Minced Garlic (or 1 tbs garlic powder)
* 1 tbs Black Pepper
* 1 tps Salt to taste (you can always add more... just hard to remove)
* Cilantro - a hand full when it is time to blend

### Optional
* 1 tbs * Chipotle in Adobo Sauce (adds a smoky flavor)

## Instructions
1. Put ingredients into a small pot
2. Add water to top of ingredients 
    * less water = thicker salsa, up to you
    * I Like a thicker salsa
3. Boil for 15 minutes
    * enough time for the chilies to turn a pale green
4. Add a good handful of fresh cilantro (after boiling)
5. With a hand blender, blend away
    * I typically blend thoroughly
    * *NOTE:* If you prefer chunky, a food processor with some pulses is ideal

Enjoy your salsa, just don’t burn yourself...
