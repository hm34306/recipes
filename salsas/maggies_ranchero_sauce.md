# Maggie's Ranchero Sauce

My mom made a great ranchero sauce. It was full of flavor, chunky and spicy! She made her ranchero sauce on the weekends and it was hit. 2 dishes that I most enjoyed

* Chicharron tacos
* Thin pan fired steak tacos with chicharron

## Difficulty

Easy

## Ingredients

* 1 tbs lard (*or preferred oil*)
* 2 medium vine Tomatoes (*diced*)
* 2 medium Onions (*diced*)
* 4 Serranos (*hotter*) or Jalapeños (not as hot) or a combination (*finely chopped*)
* 1 15 oz Crushed Tomatoes
* 2 tbs Minced Garlic (*or 1 tbs garlic powder*)
* 1 tbs Black Pepper
* 1 tps Salt to taste (*you can always add more... just hard to remove*)
* Cilantro - a hand full

## Instructions

1. Add lard and heat until reflecting
2. Put onions, serranos, garilc dn cilantro, salt and pepper
3. Cook for 2-3 minutes
4. Put in tomatoes
5. Add a little water
   * A thick ranchero sauce is what I prefer. if you put too much water, boil a little longer and let it reduce
6. Boil for 15 minutes
   * enough time for the chiles to turn a pale green

Enjoy your ranchero sauce, just don’t burn yourself...
