# Pico de Gallo
Yes, bite of the rooster! A simple, fresh chunky salsa with loads of heat and fresh flavor. When I make *Pico de Gallo* the intent is for to to be hot. This great great on barbacoas [Pork Barbacoa](../pork_barbacoa.md), fajitas, tacos. You name it, this works!

# Ingredients
* 1 Medium Onion, finley chopped
* 2 Medium Tomatoes, cored and finely diced
* 5 Thai chilies (or chili of choise), finely chopped
    * I prefer a *Pico de Gallo* with some make you sweat heat!
* 1 Medium Lime juiced
* 1/2 handful of Cilantro, finely chopped
* Salt
* Pepper

# Steps
1. In a bowl add ad the presh ingredients
2. Add Salt
3. Add some pepper, not much
4. Add lime juice
5. Fold it all together

# It's time to eat!
Dip with chips or add to any taco for instant flavor and heat!
