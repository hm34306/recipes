# Simple Green Tomatillo Salsa
Restaurants these days offer 2 salsa's. My mom's salsa was green, but only because it was 90% jalapenos. This salsa will use tomatillos and will give a slightly sour taste.

## Difficulty
Easy - Belicia, my 9 year old makes this now :)

## Ingredients
* 15 oz Tomatillos
    * Fresh - peel husks and clean
    * Can - drain water out of the can.
* 2 medium Onions cut into 6 pieces
* 4 Serranos (hotter) or Jalapeños (fresh, not as hot) or a combination
   * add more if you so dare
   * add cayenne, though it does change the flavor a bit
* 2 tbs Minced Garlic (or 1 tbs garlic powder)
* 1 tbs Black Pepper
* 1 tps Salt to taste (you can always add more... just hard to remove)
* Cilantro - about a hand full

## Instructions
1. Put ingredients into a small pot
2. Add water to top of ingredients 
    * less water = thicker salsa, up to you
    * I Like a thicker salsa
3. Boil for 15 minutes
    * enough time for the chilies to turn a pale green
4. Add a good handful of fresh cilantro (after boiling)
5. With a hand blender, blend away
    * I typically blend thoroughly
    * *NOTE:* If you prefer chunky, a food processor with some pulses is ideal

Enjoy your salsa, just don’t burn yourself...
