# Pulled Pork
This would be the American approach to barbacoa. Utlimately we eat tacos, so smoked pork but has tacos written all over it!

# To Brine or not to Brine
Pork is megga fatty and has a lot flavor on its pown, but when smoked it can get a bit dried out. If you plan ahead and can brine then do it. Its not entirely necessary. If you find you youave dried pulled pork, then brine. I'm not a brine expert and typically do not brine. There are plenty of sites that have brines and if I was going to do one I would probably take pieces from the movie Chef. A Chef inspired brine can be found [here](https://www.bingingwithbabish.com/recipes/2017/3/28/cubanos-inspired-by-chef).

## Difficulty
Easy

## Prep Time
10 Minutes

## Cook Time
~ 1.5 Hours per pound of pork or when internal termperature is 195 degrees farenheit

# Ingredients
* Boston Butt
* Pepper and Salt a ratio of 5 parts salt to 1 part salt
    * This is the rub
* Coursely Gound Black Pepper Corns

# Steps
1. Get the smoker going to 275.
2. Remove the pork from packaging and remoe excess moisture with paper towel.
3. Liberally apply rub and massage in.
4. With fat cap facing up, apply Coursely Gound Black Pepper Corns and press in.
5. When smoker is ready, add to smoker, manage your tempature.
6. When pork is at 195 Degress Farenheit, it can be pulled out.
    * You should all see that the fat cap has cracked, letting us kow it is ready
7. Put on a cutting baord and tent for ~ 45 minutes
8. Pull out your shredders and shred away.

## It's time to eat!
Grab some tortillas. Add some [Gucamole](../appetizers/gucamole.md) and [Pico de Gallo](../salsas/pico_de_gallo.md). Add some pork and a sprinkle of salt. 

You've done work, now enjoy!
