# Carnitas
There a number of ways to make carnitas. I will split this off into a multiple pathways. They each have unique great flavor so you never get bored. The flavor we want from canitas is rendered pork with some simple spices. It can be as simple as just salt or more complex seasonings.

* [Carnitas Fried](#carnitas-fried)
* [Carnitas from Barbacoa](#carnitas-from-barbacoa)
* [Carnitas Smoked](#carnitas-smoked)

Pick at it as is, or heat up some tortillas, add some [Guacamole](../appetizers/guacamole.md] and/or [Pico de Gallo](../salsas/pico_de_gallo.md) and enjoy!

# Carnitas Fried <a name="#carnitas-fried"></a>
This is simple. I do this for my kids and we'll knock a whole pok shoulder in an evening. It's rich in flavor and texture. 

## Difficulty
Easy

## Prep Time
15 minutes

## Cook Time
1 hour (frying a lot of pork)

## Ingredients
* 1 Quart of Lard
* Pork Shoulder or Boston Butt

## Steps
1. Head the lard to prepare for deep frying.
    * What the magic temperature is, I'm not sure. It's how enough to fry pork.
2. Cube sholder/butt into 1/2 inch cubes.
3. When the frier is ready, fry some pork.
    * You will want to fry it until the outside shows that it is rendering. It will have a brownish-red outside. The more rendering you have, the more flavor. If it is light brown or white, it is *not* ready.
4. When ready, remove pork and place on a paper towel and pat dow nto remove excess lard.
5. Salt the pork.
6. Continue fying the rst of your pork.

## It's time to eat!
Grab some tortillas. Add some [Gucamole](../appetizers/gucamole.md) and [Pico de Gallo](../salsas/pico_de_gallo.md). Add some pork and a sprinkle of salt. 

You've done the work, now enjoy!

### Note
Do not save the lard you fried in. It needs to thrown out, and not down the drain.

### Note
This will be a bit chewy. That is the nature of this. The other carnitas recipes use shredded pork and is easier to consume.

# Carnitas from Barbacoa <a name="#carnitas-from-barbacoa"></a>
This recipe takes [Pork barbacoa](./barbacoa.md) and renders the barbacoa in the overn. Beacuse it is shredded, it is not as chewy. Because it is shredded it renders really well. 

## Difficulty
Easy

## Prep Time
8 Hours 

## Cook Time
30 Minutes

## Steps
1. Heat over to 500 degrees farenheit
2. In large flat pan, thinly spread out [Pork barbacoa](./barbacoa.md)
3. Put in oven for 15 minutes
4. Remove from oven and salt to taste

## It's time to eat!
Grab some tortillas. Add some [Gucamole](../appetizers/gucamole.md) and [Pico de Gallo](../salsas/pico_de_gallo.md). Add some pork and a sprinkle of salt. 

You've done the work, now enjoy!

# Carnitas Smoked <a name="#carnitas-smoked"></a>
This recipe takes [Smoked Pull Pork](./smoked_pulled_pork.md) and renders the pulled pork in the overn. Beacuse it is shredded and smoked, it is extremely tender!. Boston Butt has peltny of fat and will render nicely, even after being smoked.

## Difficulty
Easy

## Prep Time
8 Hours 

## Cook Time
30 Minutes

## Steps
1. Heat over to 500 degrees farenheit
2. In large flat pan, thinly spread out [Pork barbacoa](./barbacoa.md)
3. Put in oven for 15 minutes
4. Remove from oven and salt to taste

## It's time to eat!
Grab some tortillas. Add some [Gucamole](../appetizers/gucamole.md) and [Pico de Gallo](../salsas/pico_de_gallo.md). Add some pork and a sprinkle of salt. 

You've done the work, now enjoy!