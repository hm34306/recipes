<img src="../images/chicharrones_tacos_in_ranchero_sauce.jpg"
     alt="Chicharones Tacos"
     style="float: right; margin-right: 10px; width:300px; height: 300px" />
     
# Chicharrones in Ranchero Sauce Tacos
My mom made what I feel is the best Chicharrones. She only made it on weekends. People would drive in from an hour away and wait up to 1 hour for her food on the weekends. By itself or scrambled with egg, it hits the spot every time. Full of flavor, very spicy and made me break a sweat! 

## Difficulty
Easy

## Prep Time
10 Minutes

## Cook Time
1 Hour

## Requires
[Maggies' Ranchero Sauce](../salsas/maggies_ranchero_sauce.md)

## Ingredients
* 5 oz bag of Chicharrones or Pork Rinds or both
    * Chicharrones will have some pork fat on there add to the flavor
    * Pork Rinds is just the fired pok skin... also very good in this dish
* Flour or Corn Tortillas

## Optional
* eggs

## Instructions
1. With the [Maggies' Ranchero Sauce](../salsas/maggies_ranchero_sauce.md) boiling, add the Chicharrones
    * If you want to add more Chicharrones you can. You don't it it to be too runny, but not too dry either
2. Stir it around to cover all the Chicharrones with *Maggies' Ranchero Sauce*
3. Once boiling, lower heat to simmer
4. Simmer for 45 minutes

## Tacos
Heat up your flour tortilla and add your Chicharrones. Enjoy!

## Breakfast Tacos
1. In a pan, add small bit of oil. 
2. Add some *Chicharrones in Ranchero Sauce* and let it heat up
2. Once the Chicharrones thicken (reduces water) add a couple of eggs and scramble
