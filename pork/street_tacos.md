# Savory Cilantro Rice

We all love the idea of street tacos. Everytime I smell them, I quickly move to wanting some. The good news is that they are vey simple to make and get the added benfit of fresh aromaotics to kick up hte flavor.

## Difficulty

Easy

## Ingredients

* Pork shoulder - diced in to 1/2 inch cubes (as mucha s you want)
* Onions - Diced
* Cilantro - Diced
* Radish - Dimed
* Corn or Flour Tortillas

## Instructions

1. Put pork in a fruing pan and cook down until it begins to render. If yoou like more rendered pork, then render to your liking
   
Heat up a tortilla, add your meat and garnish to your liking. Add [Guacamole](../appetizers/guacamole.md) and salsa ([../salsas]{../salsas}) to add to even more flavor!

Enjoy!

## Make it Spicier

Add another Jalapeno or try seranos. I've used a combiantion of Pablano Peppers with Jalapeno Peppers to change the flavor profile.

## Other Varations

### Add Velveeta

Melt 1/3 block of Velveeta. When rice is complete, fold in the cheese. .
