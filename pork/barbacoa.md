# Pork Barbacoa
Barbacoa is any meat that is cooked to where it is shreds. We will make a nice flavorful barbacoa in a crock pot. It may sound like the wrong thing to do, but guess what, I do not have a whole in back yard for cooking on top of coals. So we'll improvise.

## Difficulty
Easy

## Prep Time
15 Minutes

## Cook Time
8 Hours (were using a crock pot)

# Ingredients
1. Pork Shoulder or Boston Butt, cut into 1.5 inch cubes
2. 1 medium onion, diced
3. 2 Jalepenos, finely chopped
4. 2 bay leaves
5. Garlic Powder
6. Pepper
7. 1 tbs Chipotle Adobo, pureed
8. I'm making a change

# Steps
1. Turn crock pot on high and set for 8 hours
1. Put onions in crock pot
2. Put pork in crock pot
3. Add Pepper
4. Add Salt
5. Add garlic powder
6. Add Chipotle Adobo

Get your hand dirty and mix it all all up in the corkc pot.
7. Add bayleaves on top
8. Cover crock pot

Every now and then, like every 2 hours, stir the crock pot up. 

## At 7 Hours
Around 7 hours the meat should be shreddable, and you can use a potatoe smasher shred the meat. 

I also check for the falvor of salt, pepper and garlic and if I need to add more to taste, I do.

## 8 hours... Done!
Pork is fatty and the crock pot will have a lot of grease. I typically strain at least 75% of the grease and throw it away. 
Now do a final tasting and ensur ethe seasoning is where you like it.

# It's time to eat!
Grab some tortillas. Add some [Gucamole](../appetizers/guacamole.md) and [Pico de Gallo](../salsas/pico_de_gallo.md). Add some pork and a sprinkle of salt. 

You've done the work, now enjoy!