# Spanish Rice

Making my mom's spanish rice took a good while to learn. I stumbled for around five years until I finally got it. This is a comino based spanish rice and the same rice my grandma makes. It is very flavorful and I can eat it just by itelf. The aromatics when cooking aramazing!

Note: Most spanish rice that I have at resraunts is not all too flavorful and a bit tomatoe-y in flavor.

## Difficulty

Hard

## Ingredients

* 2 Cups Long Grain Rice
* 4 Cups Water
* 1 Bell Pepper (*diced*)
* 1 Medium Onion (*diced*)
* 1 8 oz can tomato sauce
* 3 tps Ground Comino
* 1 tps Black Pepper
* 1 tps Salt to taste (*you can always add more... just hard to remove*)
* 1 Bullion cube (*optional*)

## Instructions

1. Add enough olive oil to cover the bottom of the pan and heat up.
2. Add rice.
   * Stir rice and ensure rice is covered in oil.
   * Brown the rice. The more bowned the rice, the more flavor you are adding.
   * You will smell it as it browns.
3. Once browned, put onions and bellpepper.
4. Pan fry for 1-2 minutes.
5. Put in tomato sauce and let simmer for another 30 seconds.
6. Add water.
7. Add comino, salt and pepper (and bullion cube) and give a gentle stir.
8. Bring to a boil.
9. Cover pan.
10. Put on low wheat and cook for 19 minutes.

Enjoy Maggie's spanish rice!
