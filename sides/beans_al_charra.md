# Beans a la Charra

A fresh pot of beans and a fresh spot of [Spanish Rice](./spanish_rice.md) make a complete comfort meal that satisfies my tastebuds. Yummy!

## Difficulty

Medium

## Ingredients

* 4 Cups Pinto Benans
* 12 Cups Water
* 1 Bell Pepper (diced)
* 1 Medium Onion (diced)
* 1 Can Diced Tomatoes
* 1 tps Black Pepper
* 1 tps Garlic Powder
* 1/2 cup Cilanto
* 1 tps Salt to taste (*you can always add more... just hard to remove*)
* 1/2 lb Bacon (diced)

## Instructions

1. Seperate and clean beans. Sometimes you find small rocks.
2. Put beans, water, onions, bell pepper, bacon, garlic powder, and pepper in a pot and bring to boil.
3. Lower heat and continue boiling for 1.5 hours. Check every so often to add more water. Not need to stir, but to keep beans at a boil.
4. At 1.5 hours add cilantro, diced tomatoes and salt to taste.
5. Boil for an additinal 30 minutes. Pull out a bean dn pinch test. If it easily pinches, your beans are ready!

Enjoy!

## Black Beans or Pinto Beans?

I use the same recipe for both. Some will seperate the cheese cloth to seperate the veggies and pull out from black beans. Maybe I'm lazy, but I prefer not to. I end up smashing beans anyways.

## Refried Beans

If you have bacon grease of lard, ad 2 Tbsp to a pot and heat up. Add beans, bring ti a soft boil and smash. You have refreied beans!

## Bean and Cheese Tacos

Warm up tortillas, add refried beans, and some sharp cheddar cheese for a quick satisfying meal noone can turn down.

## Other Varations

Use Pablona Peppers instead of Bell Peppers.
Use 1/2 lb Chorizo or Kilobasa Sausage in place of bacon.
