# Southwestern Squash

Borowed form a restraunt by the name of Threadgill's, this is a very teatey side with a put of a kick.

## Difficulty

Easy

## Ingredients

* 2 Medium Onions - diced
* 1 Jalapeno- diced
* 4 Yellow Squafh - half moons
* 1/2 Stick of Butter
* 1/4 Block of Velveta - cubed
* Salt
* Garlic
* Pepper

## Instructions

1. Melth the butter
2. Add onions and chile and sautee until soft
3. Add Salt, Pepper and Garlic ot tase
4. Add Squash and sautee until medium soft
5. Add Velveeta chees and still until melted

You are looking for a consistancy that is not too watery. At that point is is ready and will have the right texture.

Enjoy!

## Make it Spicier

Add another Jalapeno or try seranos. I've used a combiantion of Pablano Peppers with Jalapeno Peppers to change the flavor profile.

## Other Varations

### Make is a soup!

Adding more cheese and water will give a more soupy texture that is very tastey.

### Make it a queso!

If you want a healtier take on question, adding squash and ionion wil not siddapoint. Pull out a hand blender and blend until smooth. Your guests will not flinch at the taste and you helped them be a little bit healtiher.
