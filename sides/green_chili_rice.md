# Green Chili Rice

If you are looking for a litle twist on rice, this is a great aromatic and flavor rice that can also be a cheesy addition to your meal.

## Difficulty

Easy

## Ingredients

* 2 Cups Long Grain Rice
* 4 Cups Water
* 1 Pablano Pepper (*diced*) (*Add diced Jalapeno or serano to spice things up!*)
* 1 Medium Onion (*diced*)
* 2 small can of diced Hatch green chiles
* 1 tps Black Pepper
* 1/2 cup Cilanto
* 1 tps Salt to taste (*you can always add more... just hard to remove*)
* 1 Bullion cube (*optional*)

## Instructions

1. Bring water to a boil.
2. Add Rice, Pablona Pepper, Onions, Green Chilies, Cilantro, Black Pepper, Bullion cube and Salt to taste
3. Once browned, put Onions and Bell Pepper.
4. Cover pan.
5. Put on low wheat and cook for 19 minutes.

Enjoy!

## Add Cheese!

This is a great dish to add Velveeta to. Melt 2 cups of velveeta with a dash of milk in the microwave. When the rice is finished, stir the in the melted cheese and enjoy!
