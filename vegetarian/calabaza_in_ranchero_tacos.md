# Calabaza (Squash) in Ranchero Sauce Tacos
If yo uare looking for a flavorful vegetarian dish, this fits the bill. I adpapted this from[Chicharorones in Ranchero Sauce](../pork/chicharrones_in_ranchero_tacos.md). Full of flavor, very spicy and made me break a sweat! 

## Difficulty
Easy

## Prep Time
10 Minutes

## Cook Time
1 Hour

## Requires
[Maggies' Ranchero Sauce](../salsas/maggies_ranchero_sauce.md)

## Ingredients
* 5 Yellow squash
    * Cut itno half moons
* Flour or Corn Tortillas

## Optional
* eggs

## Instructions
1. With the [Maggies' Ranchero Sauce](../salsas/maggies_ranchero_sauce.md) boiling, add the squash.
    * If you want to add more Chicharrones you can. You don't it it to be too runny, but not too dry either.
2. Stir it around to cover all the Chicharrones with *Maggies' Ranchero Sauce*.
3. Once boiling, lower heat to simmer.
4. Simmer for 30 minutes.

## Tacos
Heat up your flour tortilla and add your Chicharrones. Enjoy!

## Breakfast Tacos
1. In a pan, add small bit of oil. 
2. Add some *Calabaza in Ranchero Sauce* and let it heat up
2. Once the Calabaza thickens (reduces water) add a couple of eggs and scramble
